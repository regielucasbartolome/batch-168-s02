<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Batch 168 - Activity S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
	<h1>Divisibles of Five</h1>
	<?php divisibleByFive(); ?>

	<h1>Array Manipulation</h1>
	<p><?php print_r($studentNames); ?></p>
	<p>Original number of students: <b><?php echo count($studentNames); ?></b></p>

	<?php array_push($studentNames, 'Juan Dela Cruz'); ?>

	<p><?php print_r($studentNames); ?></p>

	<?php count($studentNames); ?>

	<p>Total number of Students: <b><?php echo count($studentNames); ?></b></p>

	<?php array_push($studentNames, 'Maria Juana'); ?>

	<p><?php print_r($studentNames); ?></p>

	<?php count($studentNames); ?>

	<p>Total number of students: <b><?php echo count($studentNames); ?></b></p>

	<p><?php array_shift($studentNames) ?></p>

	<p><?php print_r($studentNames); ?></p>

	<?php count($studentNames); ?>

	<p>Total number of students: <b><?php echo count($studentNames); ?></b></p>


	
</body>
</html>