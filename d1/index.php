<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
	<h1>Repitition Control Structures</h1>

	<h2>While Loop</h2>

	<?php whileLoop(); ?>

	<h2>Do-While Loop</h2>

	<?php doWhileLoop(); ?>

	<h2>For Loop</h2>

	<?php forLoop(); ?>

	<h2>Continue and Break</h2>

	<?php modifiedForLoop(); ?>

	<h1>Array Manipulation</h1>

	<h3>Simple Array</h3>

	<ul>
		<?php foreach($computerBrands as $brand) { ?>
			<li><?php echo $brand; ?></li>
		<?php } ?>
	</ul>

	<h3>Associative Array</h3>

	<ul>
		<?php foreach($gradePeriods as $period => $grade) { ?>
			<!-- < ?= is shortcut for < ?php echo ?> -->
			<li>Grade in <?= $period ?> is <?= $grade ?></li>
		<?php } ?>
	</ul>

	<h3>Multidimensional Array</h3>

	<ul>
		<?php foreach($heroes as $team) {
			foreach($team as $member) {
				echo '<li>'.$member.'</li>';
			}
		}?>
	</ul>

	<!-- Displaying the specific element in two-dimensional array -->
	<p><?php echo $heroes[2][2] ?></p>

	<h3>Sorting</h3>

	<!-- print_r prints human readable content of the array -->
	<pre><?php print_r($sortedBrands); ?></pre>

	<h3>Sorting Reverse</h3>

	<pre><?php print_r($reverseSortedBrands); ?></pre>

	<h3>Append</h3>

	<?php array_push($computerBrands, 'Apple') ?> <!-- add at the end -->

	<pre><?php print_r($computerBrands) ?></pre>

	<?php array_unshift($computerBrands, 'Dell') ?> <!-- add in the index 0 -->

	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Remove</h3>

	<?php array_pop($computerBrands) ?> <!-- delete last entry -->

	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_shift($computerBrands) ?>

	<pre><?php print_r($computerBrands) ?></pre>

	<h4>Count Function</h4>

	<pre><?php echo count($computerBrands) ?></pre>

	<h4>In Array</h4>

	<p><?php echo searchBrand($computerBrands, 'HP') ?></p>
	<p><?php echo searchBrand($computerBrands, 'Apple') ?></p>

	<pre><?php print_r($reverseGradePeriods) ?></pre>
</body>
</html>