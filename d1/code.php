<?php

//Repitition Control Structures

/*
	While Loop
	Do-While Loop
	For-Loop
*/

//=====While Loop=====

	//it evaluates first the condition if results to TRUE, then execute block of codes

	function whileLoop() {
		$count = 5;

		while($count !==0) {
			echo $count.'<br/>';
			$count--;
		}
	}

//=====Do-While Loop

	//execute the statement at least once and if the condition is met, it will continue to execute the statement.

	function doWhileLoop() {
		$count = 20;

		do{
			echo $count.'<br/>';
			$count--;
		}while($count > 0);
	}

//=====For Loop=====

	//more flexible compared to while and do-while
	//contains parameters initial value; condition; iteration

	function forLoop() {
		for($count = 0; $count <=20; $count++) {
			echo $count.'<br/>';
		}
	}


//=======================================//
//    Continue and Break Statement       //
//=======================================//

	/*
		continue is a keyword that allows the code to go to the next loop without finishing the current code block.

		break keyword ends the execution of the current loop
	*/

	function modifiedForLoop() {
		for($count = 0; $count <= 20; $count++) {
			if($count % 2 === 0) {
				continue;
			}
			echo $count.'<br/>';
			if($count > 10) {
				break;
			}
		}
	}

//=====Array Manipulation

	$studentNumbers = array('2020-1923', '2020-1924', '2020-1924', '2020-1924', '2020-1924');// before PHP 5.4
	$studentNumbers = ['2020-1923', '2020-1924', '2020-1924', '2020-1924', '2020-1924']; //introduced on PHP 5.4 and up

	//Simple Arrays
	$grades = [98.5, 94.3, 89.2, 90.1];
	$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'HP', 'Toshiba', 'Fujitsu'];
	$tasks = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake sass'
	];

	//Associative Array
	$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

	//Two-dimensional Array

	$heroes = [
		['Ironman', 'Thor', 'Hulk'],
		['Wolverine', 'Cyclops', 'Storm'],
		['Darna', 'Captain Barbel', 'Lastikman']
	];

	//Two-dimensional Associative Array
	$ironManPowers = [
		'regular' => ['repulsor blast', 'rocket punch'],
		'signature' => ['unibeam']
	];

	//Array Sorting

	$sortedBrands = $computerBrands;
	$reverseSortedBrands = $computerBrands;

	//Sort Arrays

	sort($sortedBrands);
	rsort($reverseSortedBrands);

//=====Other Array Functions=====

	function searchBrand($brands, $brand) {
		return (in_array($brand, $brands)) ? "$brand is in the array" : "$brand is not in the array.";
	}

	$reverseGradePeriods = array_reverse($gradePeriods);